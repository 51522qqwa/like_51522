// 元素获取
var $ = (e,parent) => {
    parent = parent || document;
    return parent.querySelector(e);
}
var $gs = (e,parent) => {
    parent = parent || document;
    return [...parent.querySelectorAll(e)];
}

const local = localStorage;
let msData = JSON.parse(local.getItem("msData")) || []  //可能没有记录为空

// 移动端封装tap函数 和 滑动函数
var myTouch = {

    // 点击
    // 点击-----------------------------------------------------------------
    // 封装个 tap 函数
    // el : 绑定事件的元素  callback 回调函数 执行什么事情
    tap : function ( el , callback ){
        if(el.nodeType === 1){
            // 1.记录是否产生移动
            var isMove = false;
            // 2.记录开始按下的时间
            var startTime = 0;
            // 3.开始触摸
            el.addEventListener( 'touchstart' , () => {
                // 记录开始时间
                startTime = new Date() * 1 ;
            })
            // 4.触摸滑动
            el.addEventListener( 'touchmove' , () => {
                // 开关开启,表示产生了滑动 并非点击
                isMove = true;
            })
            // 5.触摸结束
            el.addEventListener( 'touchend' , () => {
                // 判断
                // 1 是否发生了偏移 即 未偏移
                // 2 判断时间差值在150ms以内  即点击
                if( !isMove && new Date() * 1 - startTime <= 1000 ){
                    // 回调函数
                    callback && callback()
                }else{
                    console.log('稳住手指 不要长按');
                }
                // 开关关闭
                isMove = false;
                // 时间重新获取
                startTime = 0;
            })
        }
    },


    // 滑过
    // 滑过----------------------------------------------------------------
    // 参数 三个
    // el 在哪个元素上进行了滑过
    // direction 在哪个方向滑过
    // callback 执行什么操作    回调函数

    touch : function ( el , dir , callback ){
        if( el.nodeType === 1 ){
            // 记录开始位置 x , y 确定滑过的方向
            var startPos = null;
            // 记录结束位置
            var endPos = null;


            // 开始触摸
            el.addEventListener( 'touchstart' , e => {
                // 记录手指按下的位置
                startPos = {
                    x : e.touches[0].clientX ,
                    y : e.touches[0].clientY ,
                }
            })

            // 可以选择滑动过程 去实时给结束位置赋值    
            // 下面选择touchend   选择使用这个 e.changedTouches
            // 不选择touchmove

            // 触摸结束
            // 通过结束点 和 开始点进行对比 来决定是哪个方向进行了滑动 
            // 且 执行什么函数
            el.addEventListener( 'touchend' , e => {
                // 记录结束位置
                endPos = {
                    x : e.changedTouches[0].clientX ,
                    y : e.changedTouches[0].clientY ,
                }
                if( startPos && endPos && dir === direction( startPos , endPos ) ){
                    callback && callback()
                }
            })

            // 判断方向的函数 通过结束位置 和 开始位置
            function direction ( start , end ){
                var absX = Math.abs( start.x - end.x);
                var absY = Math.abs( start.y - end.y);
                // 手指大小不一 轻重不一  保存一个区间差值容错 30
                if( absX > 30 || absY > 30 ) {
                    // 判断方向
                    if( absX > absY ){    // 水平反向移动
                        // 用开始水平 - 结束水平 > 0 左滑 否则右滑
                        return start.x - end.x > 0 ? 'left' : 'right';
                    }else{
                        return start.y - end.y > 0 ? 'up' : 'down';
                    }
                }
            }
        }
    }
}

